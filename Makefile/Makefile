# Makefile 08 - A Makefile template for single-binary c programs

# Copyright (c) 2015, Monaco F. J. <moanco@icmc.usp.br>
#
# This file is part of POSIXeg.
#
# POSIXeg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Project data

PROJECT = foo

VERSION = 1.0

# Executable

BIN = main

# Objects

OBJECTS = main.o circle.o rectangle.o square.o

# Flags

CPP_FLAGS = -I.
C_FLAGS = 
LD_FLAGS =
CC = gcc
MAKE = make

# Other files to be distributed

EXTRADIST = 

# Install prefix

PREFIX=.

# Default target

all: $(BIN)

# Binary

main : $(OBJECTS)
	gcc $(LD_FLAGS) $(LDFLAGS) $^ -o $@

# Pattern rule for object-source dependences

%.o : %.c 
	$(CC) $(CPP_FLAGS) $(CPPFLAGS) $(C_FLAGS) $(C_FLAGS)-c $<

# Automatic object-header dependences

makefiles = $(OBJECTS:%.o=%.d) 


%.d : %.c
	$(CC) $(CPP_FLAGS) $(CPPFLAGS)  $^  -MM -MT '$(<:.c=.o) $@' $< >$@

NO_D := 
NO_D := $(or $(NO_D), $(findstring clean,$(MAKECMDGOALS)))
NO_D := $(or $(NO_D), $(findstring distclean,$(MAKECMDGOALS)))
NO_D := $(or $(NO_D), $(findstring uninstall,$(MAKECMDGOALS)))
ifeq (,$(NO_D))
include $(makefiles)
endif


# Cleaning

.PHONY : clean dist distcheck distclean

clean:
	rm -f $(OBJECTS) $(makefiles) $(BIN) 
	rm -f *~ \#*
	rm -rf $(PROJECT)

# Distribute

SOURCES = $(OBJECTS:%.o=%.c) 
HEADERS := $(shell $(CC) $(CPP_FLAGS) $(CPPFLAGS) -MM -MT " " $(SOURCES)) 
HEADERS := $(filter %.h, $(HEADERS))
HEADERS := $(sort $(HEADERS)) 

Makefile = $(MAKEFILE_LIST)

dist: $(SOURCES) $(HEADERS) $(EXTRADIST) $(Makefile)
	rm -rf $(PROJECT)
	mkdir $(PROJECT)
	cp $^ $(PROJECT)
	tar zcvf $(PROJECT)-$(VERSION).tgz $(PROJECT)

distcheck: dist 
	rm -rf $(PROJECT)
	tar zxvf $(PROJECT)-$(VERSION).tgz
	if $(MAKE) -C $(PROJECT) -f $(Makefile); then echo "\n$(PROJECT).tgz ready for distribution"; \
	else echo "\nSomething wrong with $(PROJECT).tgz"; fi; echo 

distclean:
	rm -rf $(PROJECT)
	rm -f $(PROJECT)-$(VERSION).tgz

# Install

install: all
	install -d $(PREFIX)/bin 
	cp $(BIN) $(PREFIX)/bin/

uninstall:
	rm $(PREFIX)/bin/$(BIN)
